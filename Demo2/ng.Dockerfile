FROM node as build
WORKDIR /app
COPY src/AngularApp/. ./
RUN npm install
RUN npm run build

FROM nginx as runtime
COPY --from=build /app/dist/angularapp /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]