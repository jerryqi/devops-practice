## Code Prepare
Test with spa:
```Bash
dotnet add Demo2/src/Demo2.csproj package Microsoft.AspNetCore.SpaServices.Extensions --version 3.0.0
```
Create a new controller for webapi:
```C#
using System;
using Microsoft.AspNetCore.Mvc;
namespace src.Controllers
{
    [ApiController]
    [Route("api")]
    public class MyController : ControllerBase
    {
        [HttpGet("getversion")]
        public object GetVersion()
        {
            return new { Version = "8.8.8.8" };
        }
    }
}
```
Import HttpClientModule:
```Ts
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```
Call webapi in app.component:
```Ts
export class AppComponent {
  title = 'angularapp';
  version = '';
  constructor(private http: HttpClient) {
    this.http.get<any>('api/getversion').subscribe(res => {
      console.log('Res:', res);
      this.version = res.version;
    })
  }
}
```
And show it:
```Html
<h1>title</h1>
<h2>{{version}}</h2>
```

## Local Run
To test the app can run normally outside of docker:
```Bash
cd Demo2/src/AngularApp && ngs
# Open a new terminal
dotnet run  --project Demo2/src/Demo2.csproj
```

docker-compose up --build -d
docker-compose -f Demo2/docker-compose.yml up --build -d

docker build -t demo2ng -f ng.Dockerfile .

docker run \
--name demo2ngapp \
-d -p 4200:80 \
-v /Users/jerryqi/DVolume/demo2ng/conf.d:/etc/nginx/conf.d \
-v /Users/jerryqi/DVolume/demo2ng/log:/var/log/nginx \
demo2ng

nginx -s reload

nginx -c /etc/nginx/nginx.conf -t