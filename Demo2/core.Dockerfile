FROM mcr.microsoft.com/dotnet/core/sdk as build
WORKDIR /app
COPY src/. ./
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet as runtime
WORKDIR /app
COPY --from=build /app/out ./
ENTRYPOINT ["dotnet", "Demo2.dll"]