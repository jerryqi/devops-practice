import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angularapp';
  version = '';
  constructor(private http: HttpClient) {
    this.http.get<any>('api/getversion').subscribe(res => {
      console.log('Res:', res);
      this.version = res.version;
    })
  }
}
