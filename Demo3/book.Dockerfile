FROM node as build
WORKDIR /app
COPY src/Gitbook/. ./
RUN npm install gitbook-cli -g
RUN gitbook build

FROM nginx as runtime
COPY --from=build /app/_book /usr/share/nginx/html/docs
CMD ["nginx", "-g", "daemon off;"]