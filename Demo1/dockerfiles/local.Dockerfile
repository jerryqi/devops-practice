FROM mcr.microsoft.com/dotnet/core/aspnet
WORKDIR /app
COPY Demo1/output/ ./
ENTRYPOINT ["dotnet", "Demo1.dll"]