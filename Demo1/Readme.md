## Local Run
To test the app can run normally outside of docker:
```Bash
dotnet run  --project Demo1/src/Demo1.csproj
```

## Manully Docker Serve
There are two ways to build this app.
### Local Build and Docker Serve
Local Build:
```Bash
dotnet publish Demo1/src/Demo1.csproj -c Release -o Demo1/output
```
Write Dockerfile:
```Dockerfile
FROM mcr.microsoft.com/dotnet/core/aspnet
WORKDIR /app
COPY Demo1/output/ .
ENTRYPOINT ["dotnet", "Demo1.dll"]
```
Make Image:
```Bash
docker build -t demo1-1 -f Demo1/dockerfiles/local.Dockerfile .
```
Run a Container:
```bash
docker run -d -p 5002:80 --name demo1-1app demo1-1
```
### Docker Build and Docker Serve
Create new dockerfile:
```Dockerfile
FROM mcr.microsoft.com/dotnet/core/sdk as build
WORKDIR /app
COPY Demo1/src/. ./
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet as runtime
WORKDIR /app
COPY --from=build /app/out ./
ENTRYPOINT ["dotnet", "Demo1.dll"]
```
Without local build, build image directly:
```Bash
docker build -t demo1-2 -f Demo1/dockerfiles/docker.Dockerfile .
```
Run a Container:
```bash
docker run -d -p 5003:80 --name demo1-2app demo1-2
```

## Jenkins Build with Git
Before work started, we need config Jenkins to use system variables:
1. Output system variables:
```Bash
echo $PATH
```
2. Go to Manage Jenkins -> Configure System -> Global properties -> Environment variables.
3. Use Path as Key, Step1's path as value, Add a Environment variable.
Then we create a job:
1. We build a new job in Jenkins named Demo1.
2. Add Git as Source Code Management:
```
https://gitee.com/jerryqi/devops-practice.git
```
3. Add build step with Execute Shell:
```Bash
docker build -t demo1 -f Demo1/dockerfiles/docker.Dockerfile .
docker run -d -p 5000:80 --name demo1app demo1
```
Reference:
- https://www.cnblogs.com/jerryqi/p/11785265.html