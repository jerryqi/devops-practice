# On MacOS
We'd better use brew in mac, it's fast and convinience.

## Docker
```Bash
brew cask install docker
```

## Jenkins
```Bash
# Add brew formulae
brew tap adoptopenjdk/openjdk
# Install JDK 8
brew cask install adoptopenjdk8
# Install Jenkins
brew install jenkins-lts
# Start it
brew services start jenkins-lts
```
Reference:
- http://qavalidation.com/2019/05/proper-way-to-install-jenkins-in-macos.html/
About why add a brew formulae, Reference:
- https://stackoverflow.com/questions/24342886/how-to-install-java-8-on-mac