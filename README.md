# devops-practice
DevOps Practice based on Docker and Jenkins

## Environment
Install docker and jenkins, you can see in [Install](./Install.md)

Create Solution:
```Bash
dotnet new sln
```
Create a solution for this project and add every sub project to this solution to make vscode can resolve reference.

## Tips
Remove Unused Images:
```Bash
docker rmi $(docker images -f "dangling=true" -q)
# Or
docker image prune
```
Remove Unused Volume:
```Bash
docker volume rm $(docker volume ls -f "dangling=true" -q)
```

## Demo1
Create Project:
```Bash
dotnet new mvc -n Demo1 -o Demo1/src
dotnet sln add Demo1/src/Demo1.csproj
```
This project shows how to host a single .net core app in docker. You can see the whole project in [Demo1](./Demo1)

## Demo2
Create Project:
```Bash
dotnet new webapi -n Demo2 -o Demo2/src
dotnet sln add Demo2/src/Demo2.csproj
ng new angularapp --directory Demo2/src/AngularApp
```
This project shows how to use docker-compose to build a multiple container with jenkins. You can see the whole project in [Demo2](./Demo2)

## Demo3
Create Project:
```Bash
dotnet new webapi -o Demo3/src/ApiServer
dotnet sln add Demo3/src/ApiServer/ApiServer.csproj
ng new angularapp --directory Demo3/src/WebClient
mkdir Demo3/src/Gitbook && cd Demo3/src/Gitbook && gitbook init
```
Then Create book.json in Gitbook:
```Json
{
    "title": "Demo3 Document",
    "author": "jerryqi",
    "description": "Demo3",
    "language": "en",
    "gitbook": "3.2.3",
    "styles": {
        "website": "./styles/website.css"
    },
    "structure": {
        "readme": "README.md"
    }
}
```
This project shows how to use docker-compose to build a multiple container with jenkins which based on demo2 which use a nginx as app gateway. You can see the whole project in [Demo3](./Demo3)

## Demo4
Create Project:
```Bash
dotnet new webapi -o Demo4/src/ApiServer
dotnet sln add Demo4/src/ApiServer/ApiServer.csproj
```